#include<stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <arpa/inet.h>

//global variables
bool running = true;
//prototypes
void stop_handler(int sig);
void exit_message();

int main(void){
  //sigaction
  struct sigaction sa;
  sa.sa_handler = &stop_handler;
  sigaction(SIGINT, &sa, NULL);
  sigaction(SIGTERM, &sa, NULL);
  sigaction(SIGPIPE, &sa, NULL);

  //Atexit
  int i = atexit(exit_message);

  //pipe
  int sockfd = 0;
  int intPassed;
  int received;
  
  if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf("Error : Could not create socket\n");
    return 1;
  }
  struct sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_port = htons(8080);

  if( connect(sockfd, (struct sockaddr *)&address, sizeof(address)) < 0)
  {
    printf("Error : Failed to connect\n");
    return 1;
  }

  while(running){
    received = read(sockfd, &intPassed, sizeof(int));
    printf("Received : %d\n", intPassed);
    sleep(1);
  }
  close(sockfd);

  //end pipe
  return 0;

}

void stop_handler(int sig){
  printf("%d\n", sig);
  printf("stop_handler called\n");
  running = false;
  // kill(0, SIGKILL);
}

void exit_message(){
  printf("Exit message\n");
}