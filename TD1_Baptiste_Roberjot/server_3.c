//mailto : marc-antoine.weisser@centralesupelec.fr
//depot git name : 3IF1020_baptiste_roberjot
#include<stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
//clang-7 -pthread -lm -o main main.c name_file
//ou cc fichier 
//global variables
bool running = true;
//prototypes
void stop_handler(int sig);
void exit_message();

int main(void){
  fork();
  wait(NULL); //alternative waitpid(-1, NULL, 0);

  //sigaction
  struct sigaction sa;
  sa.sa_handler = &stop_handler;
  sigaction(SIGINT, &sa, NULL);
  sigaction(SIGTERM, &sa, NULL);

  //Atexit
  int i = atexit(exit_message);

  //
  printf("Hello\n");
  while(running){
    printf("id : %d\n", getpid());
    printf("pid : %d\n", getppid());
    int randNb = rand()%100;
    printf("Rand nb : %d\n", randNb);
    sleep(1);
  }
  printf("Bye\n");

  return EXIT_SUCCESS;

}

void stop_handler(int sig){
  printf("%d\n", sig);
  printf("stop_handler called\n");
  running = false;
}

void exit_message(){
  printf("Exit message\n");
}