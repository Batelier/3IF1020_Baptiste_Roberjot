#include<stdio.h>
#include <stdlib.h>
#include <unistd.h>
//clang-7 -pthread -lm -o main main.c server_1.c
int main(void){
  printf("Hello\n");
  
  while(1){
    printf("id : %d\n", getpid());
    printf("pid : %d\n", getppid());
    int randNb = rand()%100;
    printf("Rand nb : %d\n", randNb);
    sleep(1);
  }
  printf("Bye\n");

  return EXIT_SUCCESS;

}