//mailto : marc-antoine.weisser@centralesupelec.fr
//depot git name : 3IF1020_baptiste_roberjot
#include<stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

//global variables
bool running = true;
//prototypes
void stop_handler(int sig); //close parent and child
void exit_message();

int main(void){
  //sigaction
  struct sigaction sa;
  sa.sa_handler = &stop_handler;
  sigaction(SIGINT, &sa, NULL);
  sigaction(SIGTERM, &sa, NULL);
  sigaction(SIGPIPE, &sa, NULL);

  //Atexit
  int i = atexit(exit_message);

  //pipe
  int pipefd[2];

  if(pipe(pipefd) < 0){
    exit(0);
  }
  int pid = fork();

  if(pid == -1){
    exit(0);
  }

  int n = 10;
  while(running){
    if (pid > 0){ // if PID == 0 --> Child
      printf("Parent id : %d\n", getpid());
      write(pipefd[1], &n, sizeof(int));
        
    }else if (pid == 0){
      int readOutput = read(pipefd[0], &n,sizeof(int));
      printf("Child id : %d\n", getpid()); 
      printf("id : %d\n", readOutput);
    }

    sleep(1);
  }

  close(pipefd[0]);
  close(pipefd[1]);

  return EXIT_SUCCESS;

}

void stop_handler(int sig){
  printf("%d\n", sig);
  printf("stop_handler called\n");
  running = false;
  kill(0, SIGKILL); //close parent and child when called
}

void exit_message(){
  printf("Exit message\n");
}