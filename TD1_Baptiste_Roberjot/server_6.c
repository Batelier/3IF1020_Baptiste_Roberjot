#include<stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <arpa/inet.h>

//global variables
bool running = true;
//prototypes
void stop_handler(int sig);
void exit_message();

int main(void){
  //sigaction
  struct sigaction sa;
  sa.sa_handler = &stop_handler;
  sigaction(SIGINT, &sa, NULL);
  sigaction(SIGTERM, &sa, NULL);
  sigaction(SIGPIPE, &sa, NULL); //SIGPIPE indique si la pipe a été shutdown ou non, si oui, running = false

  //Atexit
  int i = atexit(exit_message);

  //Pipe with socket
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  int confd;
  int hostl = htonl(sock);
  int hosts = htons(sock);

  struct sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(8080);//for instance

  bind(sock, (struct sockaddr *)&address, sizeof(address));
  
  listen(sock, 10);
  
  int valuePassed = 65;
  while(running){
    confd = accept(sock, (struct sockaddr*)NULL, NULL);
    write(confd, &valuePassed, sizeof(int));
    printf("Sending : %d\n", valuePassed);

    close(confd);
    sleep(1);

  }
  close(sock);
  //end pipe
  return 0;

}

void stop_handler(int sig){
  printf("%d\n", sig);
  printf("stop_handler called\n");
  running = false;
  // kill(0, SIGKILL);
}

void exit_message(){
  printf("Exit message\n");
}