#include<stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

//global variables
bool running = true;
//prototypes
void stop_handler(int sig);
void exit_message();

int main(void){
  //sigaction
  struct sigaction sa;
  sa.sa_handler = &stop_handler;
  sigaction(SIGINT, &sa, NULL);
  sigaction(SIGTERM, &sa, NULL);
  sigaction(SIGPIPE, &sa, NULL); //SIGPIPE indique si la pipe a été shutdown ou non, si oui, running = false

  //Atexit
  int i = atexit(exit_message);

  //pipe
  int fd; //fifo
  int valuePassed;
  fd = open("./fifo", O_WRONLY);

  printf("Server pid : %d\n", getpid());
  while(running){
    valuePassed = rand() % 50;
    write(fd, &valuePassed, sizeof(int));
    printf("Sending : %d\n", valuePassed);
    sleep(1);
  }
  close(fd);

  //end pipe
  return 0;

}

void stop_handler(int sig){
  printf("%d\n", sig);
  printf("stop_handler called\n");
  running = false;
  // kill(0, SIGKILL);
}

void exit_message(){
  printf("Exit message\n");
}