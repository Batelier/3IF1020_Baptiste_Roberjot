#include <iostream>
#include <vector>
#include <time.h>
#include <functional>
#include <forward_list>

//prototypes
void test_1();
void test_2();
void test_3();
void test_4();
//end prototypes

int main() {
    // test_1();
    // test_2();
    // test_3();
    test_4();

}

//part 1.1 --------------------------
void print_tab(const std::vector<int> &tab){
  std::cout << "[ ";
  for (int i = 0; i < tab.size(); i++)
  {
    std::cout << tab[i] << ' ';
  }
  std::cout << "]\n";
}

void random_tab(std::vector<int> &tab){
  for (int i = 0; i < tab.size(); i++)
  {
    tab[i] = std::rand() % 100;
  }
}

void sort_tab_1(std::vector<int> &tab){ //sorting
    int count = tab.size();
    for (int i = 0; i < tab.size(); i++)
    {
        int firstIndex = (tab.size() - count);
        int currentMinimum = tab[firstIndex];
        int indexToSwap = firstIndex;
        for (int j = firstIndex ; j < tab.size(); j++)
        {
            if (tab[j] < currentMinimum)
            {
                currentMinimum = tab[j];
                indexToSwap = j;
            }   

        }
        std::swap(tab[firstIndex], tab[indexToSwap]);
        count --;   
    }
}

void test_1(){
  std::vector<int> tab(10);
  std::srand(time(nullptr)); //new seed creation
  random_tab(tab);
  print_tab(tab);
  sort_tab_1(tab);
  print_tab(tab);
}

//part 1.2 --------------------------
bool less(int int1, int int2){
    return (int1 < int2);
}

bool greater(int int1, int int2){
    return (int1 > int2);
}

void sort_tab_2_function(std::vector<int> &tab, std::function<bool(int, int)> comparison){
    int count = tab.size();
    for (int i = 0; i < tab.size(); i++)
    {
        int firstIndex = (tab.size() - count);
        int current = tab[firstIndex];
        int indexToSwap = firstIndex;
        for (int j = firstIndex ; j < tab.size(); j++)
        {
            if (comparison(tab[j], current))
            {
                current = tab[j];
                indexToSwap = j;
            }   

        }
        std::swap(tab[firstIndex], tab[indexToSwap]);
        count --;   
    }
}

void sort_tab_2(std::vector<int> &tab, bool (*comparison)(int, int)){
    int count = tab.size();
    for (int i = 0; i < tab.size(); i++)
    {
        int firstIndex = (tab.size() - count);
        int current = tab[firstIndex];
        int indexToSwap = firstIndex;
        for (int j = firstIndex ; j < tab.size(); j++)
        {
            if (comparison(tab[j], current))
            {
                current = tab[j];
                indexToSwap = j;
            }   

        }
        std::swap(tab[firstIndex], tab[indexToSwap]);
        count --;   
    }
}

void test_2(){
    std::vector<int> tab(10);
    std::srand(time(nullptr)); //new seed creation
    random_tab(tab);
    sort_tab_2_function(tab, [](int x, int y){return x < y;});
    // sort_tab_2(tab, less);
    print_tab(tab);
    sort_tab_2_function(tab, [](int x, int y){return x > y;});
    // sort_tab_2(tab, greater);
    print_tab(tab);
}

//part 2.1 --------------------------

std::forward_list<int> random_list(int positiveInt){
    std::forward_list<int>  list;
    for (int i = 0; i < positiveInt; i++)
    {
        list.push_front(std::rand() % 100);
    }
    return list;
}

void print_list(const std::forward_list<int> &list){
    std::cout << "( ";
    for( int i : list ){
        std::cout << i << " ";
    }
    std::cout << ")\n";
}

//part 2.2 --------------------------

std::forward_list<int> map(const std::forward_list<int> &list, std::function<int(int)> fun){
    std::forward_list<int> storageList;
    std::forward_list<int> returnList;
    for(int i : list){
        storageList.push_front(fun(i));
    }
    // for(int i : storageList){  //pour remettre dans le bon ordre, + return returnList
    //     returnList.push_front(i); 
    // }
    return storageList;
}

//part 2.3 filtrage -----------------

std::forward_list<int> filtrer(const std::forward_list<int> &list, std::function<bool(int)> fun){
    std::forward_list<int> newList;
    for(int i : list){
        if(fun(i)){
            newList.push_front(i);
        }
    }
    return newList;
}

void test_3(){
    //l'ordre est inversée à cause du push front qui rajoute les résultats au début de la frwdList
    std::srand(time(nullptr));
    std::forward_list<int> list = random_list(10);
    print_list(list);
    int randomNb = std::rand() % 6;
    std::cout << "Random number : " << randomNb << "\n";
    list = map(list, [randomNb](int x) {return x * randomNb;});  
    list = filtrer(list, [](int x) {return x%2 == 0;});      
    print_list(list);
}

//part 2.4 réduction ---------------

#include <climits>
int reduce(const std::forward_list<int> &list, int inputInt,std::function<int(int, int)> fun){
    int currentInt = inputInt;
    for(int i : list){
        int lastRes = fun(currentInt, i);
        currentInt = lastRes;
    }
    return currentInt;
}

void test_4(){
    std::srand(time(nullptr));
    std::forward_list<int> list = random_list(10);
    print_list(list);
    std::function<int(int, int)> lambdaSmaller = [](int x, int y) {if (x > y) return y; else return x;};
    std::function<int(int, int)> lambdaGreater = [](int x, int y) {if (x < y) return y; else return x;};
    int smallerRes = reduce(list, INT_MAX, lambdaSmaller); //plus petit élément
    std::cout << "Smallest number : " << smallerRes << "\n";
    int greaterRes = reduce(list, INT_MIN, lambdaGreater); //plus petit élément
    std::cout << "Greatest number : " << greaterRes << "\n";

    //Smaller and greater at the same time
    int smallestNbOfList = INT_MIN;
    int greatestNbOfList = INT_MAX;
    std::function<int(int, int)> lambdaGS = [&smallestNbOfList, &greatestNbOfList](int x, int y){
        if (y > smallestNbOfList) smallestNbOfList = y;
        if (y < greatestNbOfList) greatestNbOfList = y;
        return 0;
    };
    reduce(list, INT_MIN, lambdaGS);
    std::cout << "One iteration : greatest : " << smallestNbOfList << " smallest : " << greatestNbOfList;

}
